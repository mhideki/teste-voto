require('dotenv').config()
const cron = require('node-cron');
const express = require('express');
const qs = require('qs')
const axios = require('axios');

app = express();

// Schedule tasks to be run on the server.
cron.schedule('*/10 * * * * *', function() {
    console.log('running a task every 10 seconds');
    const params = new URLSearchParams()
    params.append('selecionar', process.env.VOTO)

    const config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }
    
    axios.post('https://www.eleicoesaovivo.com.br/participando.php', params, config)
    .then((result) => {
        // Do somthing
        console.log(result.data);
    })
    .catch((err) => {
        // Do somthing
        console.log(err);
    });
});

app.listen(3000);